/*
*  �We certify that this work is entirely our own. The assessor of this project may reproduce this project and provide copies to other academic staff, and/or communicate a copy of this project to a plagiarism-checking service, which may retain a copy of the project on its database.
*/
#ifndef GLOBAL_MANAGER
#define GLOBAL_MANAGER

#include "MemManager.h"
#include <cassert>


#define BLOCK_SIZE 65536
#define MEMORY_MANAGER GlobalManager::getInstance()

class GlobalManager
{
private:
	// Singleton
	static GlobalManager* mspInstance;
	
	// Memory being managed
	byte mpBlock[BLOCK_SIZE];

public:
	// Singleton accessor, construct, destruct
	static GlobalManager* getInstance() { assert(mspInstance != nullptr); return mspInstance; }
	static void cleanupInstance();
	static void initInstance();

	
	/**
	 * \brief Prints a sequential block of memory byte by byte (seperated into blocks of 4
	 * \param start The starting index in mpBlock to begin printing from
	 * \param count How many bytes to print
	 */
	void printBlockSEQ(unsigned int start, unsigned int count);

	friend void* operator new(size_t size, GlobalManager* manager);
	friend void operator delete(void* ptr, GlobalManager* manager);
	friend void* operator new[](size_t size, GlobalManager* manager);
	friend void operator delete[](void* ptr, GlobalManager* manager);
};
#endif
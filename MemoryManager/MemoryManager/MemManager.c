/*
*  �We certify that this work is entirely our own. The assessor of this project may reproduce this project and provide copies to other academic staff, and/or communicate a copy of this project to a plagiarism-checking service, which may retain a copy of the project on its database.
*/
#include "MemManager.h"
#include <string.h>

int memInitialize(byte* block, size_t size)
{
	memset(block, UNUSED, size); // Clear block to unused flag
	MemHeader* blockHeader = (MemHeader*)block;
	blockHeader->blockFirst = blockHeader->blockLast = NULL;
	blockHeader->blockRemainingSpace = size - sizeof(MemHeader);
	return 0;
}

/**
 * \brief Acts as a constructor for a MemHeader
 * \param header A pointer to the uninitialized MemHeader
 * \param prev A pointer to the previous MemHeader
 * \param next A pointer to the next MemHeader
 * \param size the size of the allocation handled by this MemHeader
 * \return A pointer to the data handled by the MemHeader
 */
void* memAllocateHelper(MemHeader* header, MemHeader* prev, MemHeader* next, size_t size)
{
	header->size = size;
	header->prev = prev;
	header->next = next;
	byte* data = (byte*)(header + 1);
	memset(data, ALLOCATED, size);
	return data;
}

/**
 * \brief Gets the next valid memory address outside of the range of the memory managed by currentAlloc
 * \param currentAlloc The allocation which you are trying to find the address outside of
 * \return A pointer to the byte of the next valid memory address.
 */
char* getAddressAfterAlloc(MemHeader* currentAlloc)
{
	byte* address = (byte*)currentAlloc; // get the address of the current allocation
	return address 
				+ sizeof(MemHeader) // go past the memheader
				+ currentAlloc->size; // go past all the data
}

void* memAllocate(byte* block, size_t size)
{
	MemHeader* head = (MemHeader*)block;
	MemHeader* current = head->blockFirst;
	

	// First allocation check
	if (current == NULL)
	{
		// Get the address of the first unused chunk of memory
		byte* address = (byte*)head + sizeof(MemHeader);
		current = (MemHeader*)address;
		
		// update the block's header data
		head->blockFirst = current;
		head->blockLast = current;
		head->blockRemainingSpace -= size + sizeof(MemHeader);

		// reserve the space
		return memAllocateHelper(current, NULL, NULL, size);
	}

	{ // check to see if we can insert the new reserved memory before the first element of the linked list

		byte* addressAfterBlockHead = (byte*)head + sizeof(MemHeader); // get the address of the memory after the block header
	
		// find the distance between that address and the address of the "head" of the linked list of MemHeaders
		ptrdiff_t dist = (byte*)head->blockFirst - (byte*)addressAfterBlockHead;
		unsigned int sizeNeeded = sizeof(MemHeader) + size;

		// If the new alloc can fit in between the block header and the head of the linked list, insert it
		if ((size_t)dist >= sizeNeeded)
		{
			MemHeader* newHeader = (MemHeader*)addressAfterBlockHead; // get address of new header
			MemHeader* next = head->blockFirst; // get the next header

			// fix the pointers of block head and next
			next->prev = newHeader;
			head->blockFirst = newHeader;

			// reserve new memory
			return memAllocateHelper(newHeader, NULL, next, size);
		}
	}
	
	// check for in-between allocations
	for (; current->next != NULL; current = current->next)
	{
		// find the distance between 2 allocations, determine if there is enough space for the new allocation
		ptrdiff_t dist = (byte*)current->next - (byte*)current;
		size_t currentSize = sizeof(current) + current->size;
		size_t sizeNeeded = sizeof(MemHeader) + size;

		// if there is, allocate in between
		if ((size_t)dist >= currentSize + sizeNeeded)
		{
			MemHeader* newHeader = (MemHeader*)getAddressAfterAlloc(current); // get address of new header
			MemHeader* next = current->next; // get the next header
			MemHeader* prev = current; // get the previous header
			
			// fix the pointers of previous and next
			prev->next = newHeader;
			next->prev = newHeader;

			// reserve new memory
			return memAllocateHelper(newHeader, prev, next, size);
		}
	}
	
	
	// if the size requested is larger than the remaining space in the block, return null
	if (size + sizeof(MemHeader) > head->blockRemainingSpace)
		return NULL;


	// otherwise, allocate at end
	MemHeader* newHeader = (MemHeader*)getAddressAfterAlloc(current);
	head->blockRemainingSpace -= size + sizeof(MemHeader);
	head->blockLast = newHeader;
	current->next = newHeader;
	return memAllocateHelper(newHeader, current, NULL, size);

}

void memDeallocate(byte* block, void* toDeallocate)
{
	MemHeader* currentHeader = (MemHeader*)toDeallocate - 1;
	MemHeader* blockHdr = (MemHeader*)block;


	{ // fix all things pointing to the data to deallocate
		
		// if we are in the middle of the list, fix both pointers
		if (currentHeader->prev != NULL && currentHeader->next != NULL)
		{
			currentHeader->prev->next = currentHeader->next;
			currentHeader->next->prev = currentHeader->prev;
		}
		// if we are at the end of the list
		else if (currentHeader->next == NULL && currentHeader->prev != NULL)
		{
			blockHdr->blockLast = currentHeader->prev;
			blockHdr->blockRemainingSpace += sizeof(MemHeader) + currentHeader->size;
			currentHeader->prev->next = NULL;
		}
		// if we are at the beginning of the list and the list has multiple elements
		else if (currentHeader->prev == NULL && currentHeader->next != NULL)
		{
			blockHdr->blockFirst = currentHeader->next;
			currentHeader->next->prev = NULL;
		}
		// if we are at the beginning of a list with no other elements
		else
		{
			blockHdr->blockFirst = blockHdr->blockLast = NULL;
			blockHdr->blockRemainingSpace += sizeof(MemHeader) + currentHeader->size;
		}
	}

	// set the freed data to the freed flag
	memset(currentHeader, FREED, currentHeader->size + sizeof(MemHeader));
}

/*
*  �We certify that this work is entirely our own. The assessor of this project may reproduce this project and provide copies to other academic staff, and/or communicate a copy of this project to a plagiarism-checking service, which may retain a copy of the project on its database.
*/
#pragma once

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#else
typedef union MemHeader	MemHeader;
#endif

// James Keats gave me this idea
#define UNUSED 0x55 // U
#define ALLOCATED 0x41 // A
#define FREED 0x46 // F

typedef char byte;

	/**
	 * \brief A union for a memory header
	 */
	union MemHeader
	{
		// Most commonly used struct - a simple doubly linked list with a size component
		struct 
		{
			MemHeader *prev, *next;
			size_t size;
		};

		// Used at the head of a block to keep track of the remaing space, as well as the head and tail of the block
		struct
		{
			MemHeader *blockFirst, *blockLast;
			size_t blockRemainingSpace;
		};
	};
	

	

	/**
	 * \brief Initializes a block of memory to be managed
	 * \param block The block of memory to be managed
	 * \return 0 for success
	 */
	int memInitialize(byte* block, size_t size);
	
	/**
	 * \brief Reserves a chunk of memory from the block, and flags it as allocated
	 * \param block The block of memory
	 * \param size The size of the reserved block of memory
	 * \return A pointer to the reserved block
	 */
	void* memAllocate(byte* block, size_t size);
	
	/**
	 * \brief Deallocates a chunk of memory from the managed block
	 * \param block The block of memory
	 * \param toDeallocate A pointer to the chunk of memory to deallocate
	 */
	void memDeallocate(byte* block, void* toDeallocate);

#ifdef __cplusplus
}
#endif
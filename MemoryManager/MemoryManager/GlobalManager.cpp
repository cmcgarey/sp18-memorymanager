/*
*  �We certify that this work is entirely our own. The assessor of this project may reproduce this project and provide copies to other academic staff, and/or communicate a copy of this project to a plagiarism-checking service, which may retain a copy of the project on its database.
*/
#include "GlobalManager.h"
#include <stdio.h>
#include <stdlib.h>

GlobalManager* GlobalManager::mspInstance = NULL;  

void GlobalManager::printBlockSEQ(unsigned int start, unsigned int count)
{
	for (unsigned int i = start; i < count; ++i)
	{
		char* ch = mpBlock + i;
		if (i % 4 == 0 && i != 0) printf(" ");
		printf("%c", ch[0]);
	}
	printf("\n\n");
}

void GlobalManager::cleanupInstance()
{
	free(mspInstance);
}

void GlobalManager::initInstance()
{
	//init instance using placement new
	mspInstance = static_cast<GlobalManager*>(malloc(sizeof(GlobalManager) + 1));

	mspInstance->mpBlock[BLOCK_SIZE] = { 0 };
	memInitialize(mspInstance->mpBlock, BLOCK_SIZE);
}

// Global Manager overloads
void * operator new(size_t size, GlobalManager* manager)
{
	return memAllocate(manager->mpBlock, size);
}

void operator delete(void * ptr, GlobalManager* manager)
{
	memDeallocate(manager->mpBlock, ptr);
}

void* operator new [](size_t size, GlobalManager* manager)
{
	return memAllocate(manager->mpBlock, size);
}

void operator delete [](void* ptr, GlobalManager* manager)
{
	memDeallocate(manager->mpBlock, ptr);
}


//global overloads
void * operator new(size_t size)
{
	return operator new(size, MEMORY_MANAGER);
}

void operator delete(void* ptr)
{
	operator delete(ptr, MEMORY_MANAGER);
}

void* operator new [](size_t size)
{
	return operator new[](size, MEMORY_MANAGER);
}

void operator delete[](void* ptr)
{
	operator delete[](ptr, MEMORY_MANAGER);
}
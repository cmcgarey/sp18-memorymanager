/*
 *  �We certify that this work is entirely our own. The assessor of this project may reproduce this project and provide copies to other academic staff, and/or communicate a copy of this project to a plagiarism-checking service, which may retain a copy of the project on its database.
 */
#include <climits>
#include <cstdio>
#include <iostream>
#include "GlobalManager.h"


int main(int argc, char* argv[])
{
	GlobalManager::initInstance();

	char* ch6 = new char[16];

	char* ch5 = new char[4];
	ch5[0] = 'h';
	ch5[1] = 'e';
	ch5[2] = 'l';
	ch5[3] = 'p';



	std::cout << "Initialized Data: \n";
	MEMORY_MANAGER->printBlockSEQ(0, 250);

	std::cout << "Delete Block 1: \n";
	delete[] ch6;
	MEMORY_MANAGER->printBlockSEQ(0, 250);

	std::cout << "add new string: \n";
	std::string* s = new std::string("hello");
	MEMORY_MANAGER->printBlockSEQ(0, 250);

	std::cout << "delete new string: \n";
	delete s;
	MEMORY_MANAGER->printBlockSEQ(0, 250);

	GlobalManager::cleanupInstance();
	MEMORY_MANAGER->printBlockSEQ(0, 250);
	system("pause");
}
